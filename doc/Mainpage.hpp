/*!
\mainpage Miind-mpi Documentation
\section sec_intro Introduction
The motivation for \a MIIND is described in the \a MIIND white paper \cite{dekamps2006c}. Basically \a MIIND aims
to provide libaries for simulations in cognition that very small but useful, so that users are tempted to incorporate
them into their own projects. It explicitly tries to avoid forcing a modelling methodology on the user. It aims to offer
components that the user would want to write anyway, without offering much else. The components are supposed to be so
mature that they don't require many extensions anymore. Until there is an adopted standard for cognitive modelling and an
infrastructure in place for developping and maintaining software in this area, this is probably the only way to tempt
modellers into software reuse.

The components are Open Source software under a BSD licence.
\section licence Licence

MIIND is free and Open Source. It is distributed under a modified BSD license (see \ref license)

\section sec_install Installation
Use cmake and make. For more details see \ref installation.

\section sec_dependencies Dependencies
For more details see \ref dependencies

\section MPILib
see \ref MPILibDoc.
\section adaption Adaption of old Code to the new MPILib
see \ref changes
\section sec_appendix Appendix

\subsection sec_issues Known Issues

*/

